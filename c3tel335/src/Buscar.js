import React from 'react';
import './App.css';
import {useDispatch } from 'react-redux';

import {Button} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { updateTerm } from './redux/actions/termActions.js';

function Buscar() {
    const dispatch = useDispatch();
    return (
        <Button onClick={(e) => dispatch(updateTerm(e.target.value))} variant="outline-info" type="submit" >
            Buscar
        </Button>
    )
  }
  
export default Buscar;
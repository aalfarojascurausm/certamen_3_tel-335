export const ACTION_UPDATE = 'ACTION_UPDATE';
export const ACTION_LIST = 'ACTION_LIST';

export const updateTerm = (term) => {
	return {
		type: ACTION_UPDATE,
		payload:{
			term: 'http://api.telematica.xkivertech.cl:4000/products/' + term,
		}
	}
}

export const listTerm = (list) => {
	return {
		type: ACTION_LIST,
		payload:{
			list: list,
		}
	}
}
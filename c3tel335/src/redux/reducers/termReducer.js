import {ACTION_UPDATE, ACTION_LIST} from '../actions/termActions.js';

const initialState = {
    term: 'http://api.telematica.xkivertech.cl:4000/products/',
    list: []
};

const termReducer = (state = initialState, action) => {
	switch(action.type) {
		case ACTION_UPDATE:
			return {
				...state,
				term:  action.payload.term
            };
        case ACTION_LIST:
            return {
                ...state,
                list:  action.payload.list
            }
		default:
			return state;
	}
}

export default termReducer;
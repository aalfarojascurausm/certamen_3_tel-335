
import { createStore, combineReducers } from 'redux';
import termReducer from './reducers/termReducer.js';

const appReducer = combineReducers({
	termReducer: termReducer,
});

export default createStore(appReducer);
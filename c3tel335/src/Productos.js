import React, {useState, useEffect} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import axios from "axios";
import { ListGroup } from "react-bootstrap";
import { listTerm } from './redux/actions/termActions.js';

function Productos(props){
    const dispatch = useDispatch();

	const term = useSelector((store) => store.termReducer.term);
	const list = useSelector((store) => store.termReducer.list);
	
    useEffect(() => {
		axios
		.get(term, {
			headers: {
				'access-token': localStorage.getItem('token'),
			}
		})
		.then(res => {
			if(res.status){
				const data = res.data;
				dispatch(listTerm(data))
			}
		})
	}, [])
		return(
			<ListGroup>
				{list.map((p) => <ListGroup.Item>{p.brand} {p.description}</ListGroup.Item>)}
			</ListGroup>
		);
}

export default Productos;
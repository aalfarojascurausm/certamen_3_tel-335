import React from 'react';
import './App.css';
import Productos from './Productos'
import Buscar from './Buscar'

import { Navbar, Nav, Form, FormControl } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import Token from './Token';
import { Provider } from 'react-redux'
import store from './redux/store'

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom';


function App() {
  Token();
  return (
    
    <Router>
      <div className="App">
      <Provider store={store}> 
        <Navbar bg="blue" variant="blue">
          <Navbar.Brand href="#home">Navbar</Navbar.Brand>
          <Nav.Link as={Link} to="/">Inicio</Nav.Link>
          <Form inline>
            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
            <Link to="/busqueda">
              <Buscar/>
            </Link>
          </Form>
        </Navbar>
        <Switch>
          <Route path="/busqueda">
            <Productos/>
          </Route>
          <Route path="/">
            <Productos/>
          </Route>
        </Switch>
      </Provider> 
      </div>
    </Router>
  )
}

export default App;
